package todo.vm;

/**
 * This enum identifies the internal VM registers.
 */
public enum Register {
    MAIN_HAND,
    JUMP_CONDITION,
    RESOLVED_ADDRESS,
    PROGRAM_COUNTER;
}
