package todo.view.entities.level;

import todo.view.entities.Entity;

public interface Floor extends Entity {
    float getWidth();

    float getHeight();
}
