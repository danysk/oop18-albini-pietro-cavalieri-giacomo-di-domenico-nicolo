package todo.model.level.parser;

public class LevelParsingException extends RuntimeException {
    private static final long serialVersionUID = 5311368094808586622L;

    public LevelParsingException(final String message) {
        super(message);
    }

}
