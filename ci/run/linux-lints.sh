#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

source ci/utils.sh

cmd ci/time-tracking.py
