#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

source ci/utils.sh

cmd java -version
cmd ./gradlew build
cmd ./gradlew test
